<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_azure') . '/src/WebpackAzureTrait.php';

use \HostingWebhooks\BaseProcessor;
use \HostingWebhooks\WebpackAzureTrait;

/**
 * Plugin that handles Azure scale set operations.
 */
class AzureScaleSetProcessor extends BaseProcessor {

  use WebpackAzureTrait;

  /**
   * {@inheritdoc}
   */
  public function process(\stdClass $data) {
    $this->payload = $data;

    if ($this->webhookIsProcessing()) {
      $vars = [
        '@webpack' => $this->getWebpackName(),
      ];
      $this->log('Received retry webhook for @webpack', $vars);
      drupal_add_http_header('Status', '202 Accepted');
      drupal_exit();
    }

    return parent::process($data);
  }

  /**
   * Determine whether the current webhook is already processing.
   *
   * We acquire a lock, if not, to stop duplicate webhook threads from being spawned.
   *
   * @see AzureScaleSetProcessor::doProcessActions()
   */
  protected function webhookIsProcessing() {
    return !lock_acquire($this->getLockName());
  }

  /**
   * Return a semaphore unique to this scale set webhook.
   */
  protected function getLockName() {
    return $this->getWebpackName() . '::' . $this->getWebhookTimestamp();
  }

  /**
   * Return the timestamp from the current webhook.
   */
  protected function getWebhookTimestamp() {
    $this->getPayload()->context->timestamp;
  }

  /**
   * {@inheritdoc}
   */
  protected function doProcessActions() {
    $this->waitForScaleSetOperationToComplete();
    $this->removeMissingServersFromWebpack();
    $this->addScaleSetVmsToWebpack();
    lock_release($this->getLockName());
  }

}
