<?php
/**
 * @file
 * Azure-specific OAuth2 client class.
 */

namespace HostingWebhooks;

/* Load the OAuth2\Client class. */
include_once drupal_get_path('module', 'oauth2_client') . '/src/Client.php';

use \OAuth2\Client;

class AzureOAuth2Client extends Client {

  protected function getToken($data) {
    if (isset($this->params['resource'])) {
      $data['resource'] = $this->params['resource'];
    }
    return parent::getToken($data);
  }

}
