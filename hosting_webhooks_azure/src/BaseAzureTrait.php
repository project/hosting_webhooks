<?php
/**
 * @file
 * Aegir Base Azure Trait.
 */

namespace HostingWebhooks;

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/BaseWebpackTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/AddWebpackServerTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/RemoveWebpackServerTrait.php';

use \HostingWebhooks\BaseWebpackTrait;
use \HostingWebhooks\AddWebpackServerTrait;
use \HostingWebhooks\RemoveWebpackServerTrait;

/**
 * Trait that provides base functionality for Azure scale set operations.
 */
trait BaseAzureTrait {

  use BaseWebpackTrait;
  use AddWebpackServerTrait;
  use RemoveWebpackServerTrait;

  /**
   * {@inheritdoc}
   */
  protected function getLogVars() {
    $payload = $this->getPayload();
    $vars = parent::getLogVars() + [
      '@scale_op' => $payload->operation,
      '@cluster_name' => $this->getWebpackName(),
      '@cluster_type' => $this->getClusterType(),
    ];
    if (isset($this->ipAddress)) {
      $vars['@hostname'] = $this->ipAddress;
    }
    return $vars;
  }

  /**
   * {@inheritdoc}
   */
  protected function logTriggered($message = '@plugin webhook plugin triggered for @scale_op operation for the @cluster_name scale set.') {
    return parent::logTriggered($message);
  }

  /**
   * {@inheritdoc}
   */
  protected function logSuccess($message = '@plugin webhook plugin succeeded for @scale_op operation for the @cluster_name scale set.') {
    return parent::logSuccess($message);
  }

  /**
   * {@inheritdoc}
   */
  protected function logFailure($exception_message = '', $status = '500 Internal Server Error', $message = '@plugin webhook plugin failed for @scale_op operation for the @cluster_name scale set.') {
    return parent::logFailure($exception_message, $status, $message);
  }

}
