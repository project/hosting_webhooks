<?php
/**
 * @file
 * hosting_webhooks_server.webhook.inc
 */

/**
 * Implements hook_webhook_default_config().
 */
function hosting_webhooks_azure_webhook_default_config() {
  $export = array();

  $webhook = new stdClass();
  $webhook->disabled = FALSE; /* Edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '1';
  $webhook->title = 'Azure Scale Set';
  $webhook->machine_name = 'azure_scale_set';
  $webhook->description = 'Webhook for handling Azure scale set operations.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'azure_scale_set';
  $webhook->config = '';
  $webhook->enabled = TRUE;
  $export['azure_scale_set'] = $webhook;

  $webhook = new stdclass();
  $webhook->disabled = false; /* edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '3';
  $webhook->title = 'Log Azure data';
  $webhook->machine_name = 'log_azure_data';
  $webhook->description = 'Webhook for logging incoming Azure scale set data.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'log_azure_data';
  $webhook->config = '';
  $webhook->enabled = true;
  $export['log_azure_data'] = $webhook;

  $webhook = new stdclass();
  $webhook->disabled = false; /* edit this to true to make a default webhook disabled initially */
  $webhook->api_version = 1;
  $webhook->whid = '3';
  $webhook->title = 'Log Azure server data';
  $webhook->machine_name = 'log_azure_server_data';
  $webhook->description = 'Webhook for logging Azure scale set server data.';
  $webhook->unserializer = 'token_authorization';
  $webhook->processor = 'log_azure_server_data';
  $webhook->config = '';
  $webhook->enabled = true;
  $export['log_azure_server_data'] = $webhook;

  return $export;
}
