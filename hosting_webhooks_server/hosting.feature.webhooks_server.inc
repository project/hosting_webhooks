<?php
/**
 * @file
 * The hosting feature definition for hosting_webhooks_server.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_webhooks_server_hosting_feature() {
  $features['webhooks_server'] = [
    'title' => t('Webhook server integration'),
    'description' => t('Provides webhook plugins for server tasks.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_webhooks_server',
    'group' => 'experimental',
  ];

  return $features;
}
