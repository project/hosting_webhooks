<?php

/**
 * @file
 * Provision/Drush hooks for webhook server operations.
 */

/**
 * Implements drush_HOOK_pre_COMMAND() for provision-verify command.
 *
 * Add new servers to the aegir user's SSH known hosts.
 */
function drush_hosting_webhooks_server_pre_provision_verify() {
  if (!_needs_known_hosts_entries()) return;

  drush_log(dt('Adding `@hostname` to SSH known hosts.', _webhook_get_vars()));
  _add_hostname_to_known_hosts();
  _add_ip_address_to_known_hosts();
}

/**
 * Implements drush_HOOK_post_COMMAND() for provision-delete command.
 *
 * Remove deleted servers from the aegir user's SSH known hosts.
 */
function drush_hosting_webhooks_server_post_provision_delete() {
  if (!_needs_known_hosts_entries()) return;

  drush_log(dt('Removing `@hostname` from SSH known hosts.', _webhook_get_vars()));
  _remove_hostname_from_known_hosts();
  _remove_ip_address_from_known_hosts();
}

/**
 * Determine whether the current entity requires entries in known hosts.
 */
function _needs_known_hosts_entries() {
  $entity = d();
  // Only servers need hosts entries.
  if ($entity->type !== 'server') return FALSE;

  // If the server is running a known service, Aegir will need to be able to
  // SSH to it.
  if (isset($entity->http_port)) return TRUE;
  if (isset($entity->db_port)) return TRUE;

  // Default to no hosts entry.
  return FALSE;
}

/**
 * Add the server's hostname to SSH known hosts.
 */
function _add_hostname_to_known_hosts($verbose = TRUE) {
  if (_hostname_in_known_hosts()) return;

  $vars = _webhook_get_vars();

  if (!_validate_hostname($vars['@hostname'])) {
    drush_log(dt('Failed to determine valid hostname. Not adding to known hosts.'), 'warning');
    return FALSE;
  }

  if (!_is_reachable($vars['@hostname'])) {
    drush_log(dt('Hostname @hostname is unreachable. Not adding to known hosts.', $vars), 'warning');
    return FALSE;
  }

  $command = 'ssh-keyscan -H @hostname 2> /dev/null >> ~/.ssh/known_hosts';
  $success = 'Successfully added hostname `@hostname` to SSH known hosts.';
  $failure = 'Failed to add hostname `@hostname` to SSH known hosts.';
  return _webhook_run_command($command, $success, $failure, $verbose);
}

/**
 * Remove the server's hostname from SSH known hosts.
 */
function _remove_hostname_from_known_hosts($verbose = TRUE) {
  if (!_hostname_in_known_hosts()) return;
  $command = 'touch ~/.ssh/known_hosts && ssh-keygen -R @hostname';
  $success = 'Successfully removed hostname `@hostname` from SSH known hosts.';
  $failure = 'Failed to remove hostname `@hostname` from SSH known hosts.';
  return _webhook_run_command($command, $success, $failure, $verbose);
}

/**
 * Add the server's IP address to SSH known hosts.
 */
function _add_ip_address_to_known_hosts($verbose = TRUE) {
  if (_ip_address_in_known_hosts()) return;

  $vars = _webhook_get_vars();

  if (!_validate_ip_address($vars['@ip'])) {
    drush_log(dt('Failed to determine valid IP address (@ip). Not adding to known hosts.', $vars), 'warning');
    return FALSE;
  }

  if (!_is_reachable($vars['@ip'])) {
    drush_log(dt('IP address @ip is unreachable. Not adding to known hosts.', $vars), 'warning');
    return FALSE;
  }

  $command = 'ssh-keyscan -H @ip 2> /dev/null >> ~/.ssh/known_hosts';
  $success = 'Successfully added IP address `@ip` to SSH known hosts.';
  $failure = 'Failed to add IP address `@ip` to SSH known hosts.';
  return _webhook_run_command($command, $success, $failure, $verbose);
}

/**
 * Remove the server's IP address from SSH known hosts.
 */
function _remove_ip_address_from_known_hosts($verbose = TRUE) {
  if (!_ip_address_in_known_hosts()) return;
  $command = 'touch ~/.ssh/known_hosts && ssh-keygen -R @ip';
  $success = 'Successfully removed IP address `@ip` from SSH known hosts.';
  $failure = 'Failed to remove IP address `@ip` from SSH known hosts.';
  return _webhook_run_command($command, $success, $failure, $verbose);
}

/**
 * Run a given command and optionally log output.
 */
function _webhook_run_command($command, $success, $failure, $verbose) {
  if (!_webhook_command_exists($command)) return FALSE;

  $vars = _webhook_get_vars();
  $command = strtr($command, $vars);
  if (drush_shell_exec($command)) {
    if (!$verbose) return TRUE;
    drush_log(dt($success, $vars), 'success');
    if ($output = drush_shell_exec_output()) {
      drush_log(implode("\n", $output));
    }
    return TRUE;
  }
  else {
    drush_log(dt($failure, $vars), 'warning');
    if ($output = drush_shell_exec_output()) {
      drush_log(implode("\n", $output), 'warning');
    }
    return FALSE;
  }
}

/**
 * Return a keyed array containing the hostname and IP address of the server.
 */
function _webhook_get_vars() {
  static $vars;
  if (!isset($vars)) {
    $hostname = d()->remote_host;
    $vars = [
      '@hostname' => $hostname,
      '@ip' => _get_ip_address_from_hostname($hostname),
    ];
  }
  return $vars;
}

/**
 * Check that a given command exists on the system.
 */
function _webhook_command_exists($command) {
  // @TODO
  return TRUE;
}

/**
 * Return the IP address associated to a given hostname.
 */
function _get_ip_address_from_hostname($hostname) {
  $ip = gethostbyname($hostname);
  return _validate_ip_address($ip);
}

/**
 * Determine whether a given IP address is valid.
 */
function _validate_ip_address($ip = '') {
  if (empty($ip)) {
    $ip = _webhook_get_vars()['@ip'];
  }
  $ip = filter_var($ip, FILTER_VALIDATE_IP);
  if (empty($ip)) return FALSE;

  return $ip;
}

/**
 * Determine whether a given IP address is valid.
 */
function _validate_hostname($hostname = '') {
  if (empty($hostname)) {
    $hostname = _webhook_get_vars()['@hostname'];
  }

  return filter_var($hostname, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME);
}

/**
 * Determine whether a given host is reachable.
 */
function _is_reachable($host) {
  $port = '22';
  $timeout = '20';
  $fp = @fsockopen($host, $port, $errno, $errstr, $timeout);

  if (!$fp) return FALSE;
  return TRUE;
 }

/**
 * Determine whether a hostname is in the aegir user's known_hosts.
 */
function _hostname_in_known_hosts() {
  $command = strtr('ssh-keygen -F @hostname', _webhook_get_vars());
  return drush_shell_exec($command);
}

/**
 * Determine whether an IP address is in the aegir user's known_hosts.
 */
function _ip_address_in_known_hosts() {
  $command = strtr('ssh-keygen -F @ip', _webhook_get_vars());
  return drush_shell_exec($command);
}
