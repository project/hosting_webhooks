<?php
/**
 * @file
 * Aegir Delete Server Trait.
 */

namespace HostingWebhooks;

/**
 * Trait that provides functionality to delete Aegir servers.
 */
trait DeleteServerTrait {

  /**
   * Delete an Aegir server node.
   */
  protected function deleteServer() {
    try {
      $server = $this->getServerNode();
    }
    catch (\Exception $e) {
      return $this->logFailure($e->getMessage(), '409 Conflict', 'Could not determine which server to delete.');
    }

    try {
      hosting_add_task($server->nid, 'delete');
    }
    catch (\Exception $e) {
      return $this->logFailure($e->getMessage());
    }
  }

}
