<?php
/**
 * @file
 * Webhook processor plugin class.
 */

/* Load classes and/or traits. */
include_once drupal_get_path('module', 'hosting_webhooks') . '/src/BaseProcessor.php';
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/BaseWebpackTrait.php';
include_once drupal_get_path('module', 'hosting_webhooks_webpack') . '/src/AddWebpackServerTrait.php';

use \HostingWebhooks\BaseWebpackTrait;
use \HostingWebhooks\AddWebpackServerTrait;
use \HostingWebhooks\BaseProcessor;

/**
 * Plugin that creates an Aegir server.
 */
class AddWebpackServerProcessor extends BaseProcessor {

  use BaseWebpackTrait;
  use AddWebpackServerTrait;

  /**
   * {@inheritdoc}
   */
  protected function doProcessActions() {
    $this->AddServerToWebpack();
  }

}
