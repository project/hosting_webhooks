---
title: TokenAuthorizationUnserializer
slug: token-authorization-unserializer

---

The [TokenAuthorizationUnserializer](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/src/TokenAuthorizationUnserializer.php) class is a webhook unserializer that secures webhooks by requiring an authorization token be passed as a URL parameter by the incoming webhook.

It is covered by the following tests:
* [log-azure-server-data-webhook.feature](../../../tests/log-azure-server-data-webhook)
* [tokenauth-webhook.feature](../../../tests/tokenauth-webhook)
* [create-server-webhook.feature](../../../tests/create-server-webhook)
* [log-server-data-webhook.feature](../../../tests/log-server-data-webhook)
* [remove-webpack-server-webhook.feature](../../../tests/remove-webpack-server-webhook)
* [log-azure-scaleout-webhook.feature](../../../tests/log-azure-scaleout-webhook)
* [delete-server-webhook.feature](../../../tests/delete-server-webhook)
* [vagrant-webpack-webhooks.feature](../../../tests/vagrant-webpack-webhooks)
* [log-webpack-data-webhook.feature](../../../tests/log-webpack-data-webhook)
* [add-webpack-server-webhook.feature](../../../tests/add-webpack-server-webhook)
* [log-azure-scalein-webhook.feature](../../../tests/log-azure-scalein-webhook)
* [vagrant-server-webhooks.feature](../../../tests/vagrant-server-webhooks)
