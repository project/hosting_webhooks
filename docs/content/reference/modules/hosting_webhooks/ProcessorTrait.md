---
title: ProcessorTrait
slug: processor-trait

---

The [ProcessorTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/src/ProcessorTrait.php) trait provides re-usable functionality related processing webhooks.

Is uses the [LoggerTrait](../logger-trait) trait.

It is used in the following class:
* [BaseProcessor](../base-processor)
