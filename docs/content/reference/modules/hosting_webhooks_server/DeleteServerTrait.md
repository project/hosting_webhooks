---
title: DeleteServerTrait
slug: delete-server-trait

---

The [DeleteServerTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_server/src/DeleteServerTrait.php) trait provides re-usable functionality related to deleting Aegir servers.

It is used in the following classes and traits:
* [DeleteServerProcessor](../delete-server-processor)
* [BaseWebpackTrait](../../hosting_webhooks_webpack/base-webpack-trait)
