---
title: hosting_webhooks_example
weight: 20

---

This [module](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/tree/7.x-1.x/hosting_webhooks_example) provides webhook components and examples to log webhook data.

{{% children %}}
