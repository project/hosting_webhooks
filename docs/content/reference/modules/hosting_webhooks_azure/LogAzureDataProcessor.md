---
title: LogAzureDataProcessor
slug: log-azure-data-processor

---

The [LogAzureDataProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/LogAzureDataProcessor.php) class is a webhook processor that logs Azure data. It is intended to be used to debug production environments without initiating any Hosting tasks.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [WebpackAzureTrait](../webpack-azure-trait) trait.

It is covered by the following test:
* [log-azure-scaleout-webhook.feature](../../../tests/log-azure-scaleout-webhook)
* [log-azure-scalein-webhook.feature](../../../tests/log-azure-scalein-webhook)
