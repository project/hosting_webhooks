---
title: ScaleSetAzureTrait
slug: scale-set-azure-trait

---

The [ScaleSetAzureTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_azure/src/ScaleSetAzureTrait.php) trait provides functionality for Azure scale set operations.

It is used in the following classes and traits:
* [WebpackAzureTrait](../webpack-azure-trait)
