---
title: AddWebpackServerProcessor
slug: add-webpack-server-processor

---

The [AddWebpackServerProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_webpack/src/AddWebpackServerProcessor.php) class is a webhook processor that adds an Aegir server to a webpack cluster. If either the Aegir server or webpack node does not yet exist, it will create one or both.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [BaseWebpackTrait](../base-webpack-trait) and [AddWebpackServerTrait](../add-webpack-server-trait) traits.

It is covered by the following tests:
* [vagrant-webpack-webhooks.feature](../../../tests/vagrant-webpack-webhooks)
* [add-webpack-server-webhook.feature](../../../tests/add-webpack-server-webhook)
