---
title: BaseWebpackTrait
slug: base-webpack-trait

---

The [BaseWebpackTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_webpack/src/BaseWebpackTrait.php) trait provides functionality shared between [AddWebpackServerProcessor](../add-webpack-server-processor) and [RemoveWebpackServerProcessor](../remove-webpack-server-processor).

It uses the [BaseServerTrait](../../hosting_webhooks_server/base-server-trait), [CreateServerTrait](../../hosting_webhooks_server/create-server-trait) and [DeleteServerTrait](../../hosting_webhooks_server/delete-server-trait) traits.

It is used in the following classes and traits:
* [AddWebpackServerProcessor](../add-webpack-server-processor)
* [RemoveWebpackServerProcessor](../remove-webpack-server-processor)
* [LogWebpackDataProcessor](../log-webpack-data-processor)
* [BaseAzureTrait](../../hosting_webhooks_azure/base-azure-trait)
