---
title: LogWebpackDataProcessor
slug: log-webpack-data-processor

---

The [LogWebpackDataProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_webpack/src/LogWebpackDataProcessor.php) class is a webhook processor that logs webpack cluster data. It is intended to be used to debug production environments without initiating any Hosting tasks.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [BaseWebpackTrait](../base-webpack-trait) trait.

It is covered by the following test:
* [log-webpack-data-webhook.feature](../../../tests/log-webpack-data-webhook)
