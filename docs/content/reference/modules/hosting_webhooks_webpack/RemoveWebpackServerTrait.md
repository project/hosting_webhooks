---
title: RemoveWebpackServerTrait
slug: remove-webpack-server-trait

---

The [RemoveWebpackServerTrait](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_webpack/src/RemoveWebpackServerTrait.php) trait provides re-usable functionality related to removing Aegir servers from webpack clusters.

It is used in the following classes and traits:
* [RemoveWebpackServerProcessor](../remove-webpack-server-processor)
* [BaseAzureTrait](../../hosting_webhooks_azure/base-azure-trait)
