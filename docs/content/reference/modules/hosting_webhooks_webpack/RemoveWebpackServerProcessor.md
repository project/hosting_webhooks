---
title: RemoveWebpackServerProcessor
slug: remove-webpack-server-processor

---

The [RemoveWebpackServerProcessor](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/blob/7.x-1.x/hosting_webhooks_webpack/src/RemoveWebpackServerProcessor.php) class is a webhook processor that removes an Aegir server from a webpack cluster.

It extends the [BaseProcessor](../../hosting_webhooks/base-processor) class, and uses the [BaseWebpackTrait](../base-webpack-trait) and [RemoveWebpackServerTrait](../remove-webpack-server-trait) traits.

It is covered by the following tests:
* [vagrant-webpack-webhooks.feature](../../../tests/vagrant-webpack-webhooks)
* [remove-webpack-server-webhook.feature](../../../tests/remove-webpack-server-webhook)
