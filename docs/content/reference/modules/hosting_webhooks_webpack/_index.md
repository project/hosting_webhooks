---
title: hosting_webhooks_webpack
weight: 40

---

This [module](https://gitlab.com/consensus.enterprises/aegir/hosting_webhooks/-/tree/7.x-1.x/hosting_webhooks_webpack) provides webhooks and re-useable code for adding Aegir servers to and removing them from webpacks.

{{% children %}}
