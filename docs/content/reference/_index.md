---
title: Reference guides
weight: 40

---

This section provides technical descriptions of the project architecture and various components.

{{% children depth="2" %}}
