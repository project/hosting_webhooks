---
title: Make targets
weight: 40

---

The Azure [example test environment](/tutorials/setup-azure-test-environment) makes available the following [GNU Make](https://www.gnu.org/software/make/) targets (refer to the [example makefile](https://gitlab.com/consensus.enterprises/aegir/aegir-webhooks-dev-vm/-/blob/master/azure/Makefile)):

* `make aegir-image`: Use Packer to build an Aegir server VM image in Azure that stops short of the "Site Install" stage (which has to be done per-server). If the Aegir server image already exists. forcibly recreate it. Also create an SSH key for the aegir user, which is downloaded into the `.secret` directory.

* `make minion-image`: Use Packer to build a minion webpack node VM image in Azure, using the [consensus.aegir-minion](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-minion) Ansible role. If the minion image exists, forcibly recreate it. Also insert the Aegir server SSH key from the `.secret` directory into the minion `aegir` user's `authorized_keys` within the image. 

* `make aegir-server`: Provision a server based on the Aegir server image.

* `make scaleset`: Create a scale set with public IP per VM, using the minion webpack node image.

* `make provision-aegir-server`: Only provision, do not configure, the Aegir server. Useful for troubleshooting.

* `make configure-aegir-server`: Only configure and already-provisioned Aegir server. Useful for troubleshooting.

* `make clean-aegir-server`: Delete the Aegir server VM. 

* `make clean-scaleset`: Delete the scaleset.

* `make clean`: Careful! Delete everything.
